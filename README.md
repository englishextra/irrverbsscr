# irrverbsscr

*English Irregular Verbs Windows Screensaver transcribed and translated into Russian*

[![SourceForge](https://img.shields.io/sourceforge/dm/iv-scr-en-ru.svg)](https://sourceforge.net/projects/iv-scr-en-ru/)

[Demo Website](https://irrverbsscr.herokuapp.com/)

## Remotes

* [GitHub](https://github.com/englishextra/irrverbsscr)
* [BitBucket](https://bitbucket.org/englishextra/irrverbsscr)
* [GitLab](https://gitlab.com/englishextra/irrverbsscr)
* [CodePlex](https://irrverbsscr.codeplex.com/SourceControl/latest)
* [SourceForge](https://sourceforge.net/p/iv-scr-en-ru/code)

## Copyright

��[github.com/englishextra](https://github.com/englishextra), 2015-2018

